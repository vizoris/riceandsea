$(function() {



// Фиксированное меню при прокрутке страницы вниз
if ($(window).width() >= '991'){
$(window).scroll(function(){
        var sticky = $('.header'),
            scroll = $(window).scrollTop();
        if (scroll > 200) {
            sticky.addClass('header-nav__fixed');
        } else {
            sticky.removeClass('header-nav__fixed');
        };
    });
}

// Бургер меню
$(".burger-menu").click(function(){
  $(this).toggleClass("active");
  $('.header').toggleClass("active");
  $('.main-menu').fadeToggle(200);
});


// Баннер слайдер
$('.banner-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    speed: 1000,
    dots: true,
    arrows: true,
    fade: true,
    adaptiveHeight: true,
    responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
      }
    }
  ]
});


// Клик по корзине в шапке
$('.header-cart__wrap').click(function(){
    $('.header-cart__modal').fadeToggle();
});


// Лайк продукта
$('.product-item__like').click(function(){
    $(this).toggleClass('active');
});   


$('.faq-header').click(function(){
  if(!($(this).next().hasClass('active'))){
    $('.faq-body').slideUp().removeClass('active');
    $(this).next().slideDown().addClass('active');
  }else{
    $('.faq-body').slideUp().removeClass('active');
  };

  if(!($(this).parent('.faq-item').hasClass('active'))){
    $('.faq-item').removeClass("active");
    $(this).parent('.faq-item').addClass("active");
  }else{
    $(this).parent('.faq-item').removeClass("active");
  };
});


// FansyBox
 $('.fancybox').fancybox({});


//Табы
$('.profile-nav a:not(.profile-nav__logout)').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $('.profile-wrap').find('#' + _id);
    $('.profile-nav a').removeClass('active');
    $(this).addClass('active');
    $('.profile-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});


$('.profile-mobile__btn').click(function() {
    var _targetElementParent = $(this).parent('.profile-item');
    _targetElementParent.toggleClass('active-m');
    _targetElementParent.find('.profile-item__body').slideToggle(600);
    $('.profile-mobile__btn').parent('.profile-item').removeClass('active');
    return false;
});


// Календарь
$(function () {
    $('#datetimepicker').datetimepicker({
        locale: 'ru',
        format: 'DD/MM/YYYY'

    });
});


// Открыть меню юзера в шапке
$('.header-user > span').click(function() {
    $('.header-user__menu').fadeToggle()
});



// Показать скрытые товары в кабинете юзера
$('.bill-item__hidden--show').click(function() {
    $(this).toggleClass('active');
    $('.bill-item__hidden').toggleClass('active')
});



// Стилизация селектов
$('select').styler();


// меню в футере на мобильном
if ($(window).width() < 768) {
$('.footer-menu h4').click(function() {
    $(this).toggleClass('active');
    $(this).next('ul').fadeToggle();
});
}


// история заказов на мобильном
if ($(window).width() < 768) {
$('.bill-header').click(function() {
    $(this).toggleClass('active');
    $(this).next('.bill-wrap').fadeToggle();
});
}


if ($(window).width() > 991) {
// Движение блока "Ваш заказ"
window.onload = function() { 
  $('.airSticky').airStickyBlock();
};
}







})